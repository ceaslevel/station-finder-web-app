# see https://lithic.tech/blog/2020-05/makefile-dot-env/
ifneq (,$(wildcard ./.env))
    include .env
    export
endif

current_dir_host := $(shell pwd)
host_app_path := $(current_dir_host)/$(APP_NAME)/

# see https://lithic.tech/blog/2020-05/makefile-wildcards/
guard-%:
	@if [ -z '${${*}}' ]; then echo 'ERROR: variable $* not set' && exit 1; fi

env_guard: guard-WEB_ROOT guard-APP_NAME \
 				guard-PROJECT_NAME guard-NODE_IMAGE_NAME \
				guard-NODE_VOLUME_NAME guard-NODE_CONTAINER_NAME \

docker: docker-compose.yaml Dockerfile

init: env_guard
	mkdir $(APP_NAME) $(APP_NAME)/src
	touch $(APP_NAME)/src/index.ts

build: 
	@printf 'Building server image\n***\n'
	docker build . -t $(NODE_IMAGE_NAME)  --no-cache

install: env_guard docker	
	@echo --modules-folder $(WEB_ROOT)/node_modules/ | \
		tee $(host_app_path)/.yarnrc

	docker volume create --name $(NODE_VOLUME_NAME)

	docker run -it --rm \
		--mount 'type=volume,src=$(NODE_VOLUME_NAME),dst=$(WEB_ROOT)/node_modules/' \
		--mount 'type=bind,src=$(host_app_path),dst=$(WEB_ROOT)/$(APP_NAME)/' \
		--user $(USER):$(USER) \
		--name $(NODE_CONTAINER_NAME) \
		--workdir $(WEB_ROOT)/$(APP_NAME)/ \
		$(NODE_IMAGE_NAME) \
			/bin/sh -c \
			"yarn init -y && \
			yarn add -D typescript nodemon ts-node \
			@types/node @types/express && \
			yarn add express"

setup: init build install

cli: env_guard docker
	docker run -it --rm \
		--mount 'type=volume,src=$(NODE_VOLUME_NAME),dst=$(WEB_ROOT)/node_modules/' \
		--mount 'type=bind,src=$(host_app_path),dst=$(WEB_ROOT)/$(APP_NAME)/' \
		--user $(USER):$(USER) \
		--name $(NODE_CONTAINER_NAME) \
		--workdir $(WEB_ROOT)/$(APP_NAME) \
		$(NODE_IMAGE_NAME) \
			/bin/sh

server: env_guard docker
	docker-compose up --build

client: env_guard docker
	docker exec -it -u $(USER) $(NODE_CONTAINER_NAME) /bin/sh -c "cd client && yarn start"

devfront:
	docker-compose up -d --build
	docker exec -it -u $(USER) $(NODE_CONTAINER_NAME) /bin/sh -c "cd client && yarn start"

devcli: env_guard docker
	docker exec -it -u $(USER) $(NODE_CONTAINER_NAME) /bin/sh

alldown: docker
	docker-compose down --rmi all --volumes --remove-orphans

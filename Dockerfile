FROM node:14.17.3-alpine3.11

ARG WEB_ROOT=web APP_NAME=app USER=node

# Web layer prevents having node modules in mount path, and
# .yarnrc makes sure that node knows where to install.
# This is done currently both on the host and in the container...
RUN mkdir $WEB_ROOT $WEB_ROOT/node_modules $WEB_ROOT/$APP_NAME && \
	chown -R $USER:$USER /web && \
	echo --modules-folder $WEB_ROOT/node_modules | \
		tee $WEB_ROOT/$APP_NAME/.yarnrc

USER $USER:$USER

ENV PATH "/web/node_modules/.bin:$PATH"

COPY --chown=$USER:$USER .env /web/

COPY --chown=$USER:$USER ./app/ /web/app
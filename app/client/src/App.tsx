import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

interface Station {
  x: number
  y: number
  z: number
}

const url: string = 'http://localhost:3000/api/stations/'

function getStations(url: string): Promise<any> {
  return fetch(url)
  // .then(res => res.json())
  .then(res => {
    return res.json()
  })
}

const stations = async () => {
    try {
      const { data } = await getStations(url)
      console.log(data)
    }
    catch (err) {
      console.error(err)
    }
  }

function App() {
  useEffect(() => {stations()})
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Moi
        </p>
        {/*<stations />*/}
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;

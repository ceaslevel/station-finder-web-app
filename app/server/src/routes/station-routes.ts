import { Router } from 'express'
import * as stationController from '../controllers/station-controller'

const stationRouter = Router()

stationRouter.get('/', stationController.findAll)

export { stationRouter }
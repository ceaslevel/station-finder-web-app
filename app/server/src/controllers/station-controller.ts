import { Station } from '../models/station'
import { RequestHandler } from 'express'

export const findAll:RequestHandler = async (req, res) => {
	try {
		res.json({
			message: 'Success',
			data: await Station.findAll(),
		})
	}
	catch(err) {
		console.error(err)
	}
}

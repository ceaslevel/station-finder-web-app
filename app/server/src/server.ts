import * as dotenv from 'dotenv'
import express from 'express'
import { stationRouter } from './routes/station-routes'

dotenv.config({path: '/web/.env'})

const port = process.env.NODE_PORT || 3001
const app:express.Application = express()

app.use(express.json())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/api/stations', stationRouter)

app.get('/', (req, res) => {
	res.send('Moi')
})

app.listen(port, () => {
	console.log(`Listening on ${port}`)
})